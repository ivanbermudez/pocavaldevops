package POC.aval2.clases;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Utils {
	
	public WebDriver driver;
	public WebDriverWait wait;
	public ResourceBundle prop;
	public int waitingTime=60;
	public String screenshotFolder="logs/screenshots/";
	public String logFolder="logs/screenshots/";
	public final Logger LOGGER = Logger.getLogger(Utils.class.getName());
	private String configFile;
	public String fecha="";
	
	public Utils() {
		configFile="recursos";
		prop=cargarPropiedades(configFile);
		System.setProperty("webdriver.chrome.driver", prop.getString("rutaDriver"));
		//driver=new ChromeDriver();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-hhmm");
        Date date = new Date();
		fecha=dateFormat.format(date);
	}
	
	public void startURL(String url) {
		driver.get(url);
	}
	
	public ResourceBundle cargarPropiedades(String archivo) {
		ResourceBundle rb = ResourceBundle.getBundle(archivo);
		return rb;
	}
	
	public void screenshot(String descripcion) {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(screenshotFolder+"/"+fecha+"/"+descripcion+".png"));
		} catch (IOException e) {
			System.err.println("No se pudo tomar el pantallazo "+e.getMessage());
		}
	}
	
	public void waitObject(String wb,int waitTime) {
        wait = new WebDriverWait(driver, waitTime);
        try {
        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(wb)));
        }catch(Exception e) {
        	System.err.println("No se pudo cargar el objeto "+e.getMessage());
        }
	}
	
	public WebElement objeto(String xpath) {
		WebElement elemento=driver.findElement(By.xpath(xpath));
		return elemento;
	}
	
	public void waitUntilGone(String xpath,int waitTime) {
		wait = new WebDriverWait(driver, waitTime);
        try {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
        }catch(Exception e) {
        	System.err.println("No se pudo cargar el objeto "+e.getMessage());
        }
	}
	
	public void scroll() {
		JavascriptExecutor jsx = (JavascriptExecutor)driver;
		jsx.executeScript("window.scrollBy(0,250)", "");
	}
	
	 public WebElement expandRootElement(WebElement element) {
			WebElement ele = (WebElement) ((JavascriptExecutor) driver)
	.executeScript("return arguments[0].shadowRoot",element);
			return ele;
		}
}
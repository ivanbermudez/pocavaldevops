package POC.aval2.clases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ResourceBundle;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * Hello world!
 *
 */
public class Main 
{
	public static String configFile;
	
	public static ResourceBundle prop;
	public static Utils ut;
	public String url;
	
    public static void main( String[] args )
    {
    	Main.iniciarAplicacion();
    	Main.login();
    	Main.agregarObjeto("Gel Limpiador", 14);
    	Main.agregarObjeto("Acondicionador", 5);
    	Main.cancelarPedido("Acondicionador");
    	Main.realizarPedido();
    	//mn.home();
    	//mn.verPedidos();
    }
    
    public Main() {
    	configFile="main";
    	ut=new Utils();
    	prop=ut.cargarPropiedades(configFile);
    }
    
    public static void iniciarAplicacion() {
    	ut.startURL(prop.getString("url").replace("\"", ""));
    }
    
    public static void login() {
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/page-login/ion-content/div[2]/app-form-login/form/ion-item[1]/ion-input/input", 10);
    	ut.screenshot("login");
    	WebElement usuario=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/page-login/ion-content/div[2]/app-form-login/form/ion-item[1]/ion-input/input");
    	WebElement password=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/page-login/ion-content/div[2]/app-form-login/form/ion-item[2]/ion-input/input");
    	WebElement boton=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/page-login/ion-content/div[2]/app-form-login/form/div/button");
    	usuario.sendKeys("admin");
    	password.sendKeys("admin");
    	boton.click();
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab1/ion-content/h1", 20);
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.err.println("Error "+e.getMessage());
		}
    	/*WebElement continuar=ut.objeto("//*[@id=\"ion-overlay-2\"]/div/div[3]/button/span");
    	ut.screenshot("continuar");
    	continuar.click();*/
    	
    }
    
    public static void agregarObjeto(String producto, int cantidad) {
    	home();
    	ut.waitObject("//*[@id=\"tab-button-catalogo\"]", 20);
    	WebElement nuestroCatalogo=ut.objeto("//*[@id=\"tab-button-catalogo\"]");
    	nuestroCatalogo.click();
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/div[1]/ion-searchbar/div/input", 20);
    	ut.screenshot("nuestro catalogo");
    	WebElement buscarProducto=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/div[1]/ion-searchbar/div/input");
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		}
    	buscarProducto.sendKeys(Keys.CONTROL + "a");
    	buscarProducto.sendKeys(Keys.DELETE);
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
    	buscarProducto.sendKeys(producto);
    	buscarProducto.click();
    	ut.screenshot("buscar producto");
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/div/ion-card[1]/ion-card-header/img", 20);
    	ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/h1").click();
    	try {
			Robot rb=new Robot();
			for(int i=0;i<20;i++) {
				rb.keyPress(KeyEvent.VK_DOWN);
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	WebElement slider=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/div/ion-card[1]/ion-card-content/ion-item/ion-range");
		WebElement shadowRoot1 = ut.expandRootElement(slider);
		WebElement root2 = shadowRoot1.findElement(By.cssSelector("div[role='slider']"));
		root2.click();
		int contador=1;
		while(contador<cantidad) {
			try {
				Robot rb=new Robot();
				rb.keyPress(KeyEvent.VK_RIGHT);
			} catch (AWTException e) {
				e.printStackTrace();
			}
			root2 = shadowRoot1.findElement(By.cssSelector("div[role='slider']"));
			contador++;
		}
		//String atributo=root2.getAttribute("aria-valuenow");
		WebElement agregarCarrito=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab2/ion-content/div/ion-card[1]/ion-card-content/button");
		agregarCarrito.click();
		ut.screenshot("agregarCarrito");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			Robot rb=new Robot();
			for(int i=0;i<20;i++) {
				rb.keyPress(KeyEvent.VK_UP);
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static void home() {
    	ut.waitObject("//*[@id=\"tab-button-inicio\"]", 10);
    	ut.objeto("//*[@id=\"tab-button-inicio\"]").click();
    }
    
    public static void carritoCompras() {
    	ut.waitUntilGone("//*[@id=\"ion-overlay-8\"]//div/div/div", 20);
    	home();
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab1/ion-content/div/ion-card[2]", 20);
    	WebElement carrito=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab1/ion-content/div/ion-card[2]");
    	carrito.click();
    	ut.screenshot("carrito compras");
    }

    public static void cancelarPedido(String producto) {
    	carritoCompras();
    	WebElement listaProductos=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab3/ion-content/ion-list");
		WebElement precio=listaProductos.findElement(By.xpath(".//*[text()[contains(.,'"+producto+"')]]"));
		WebElement parent=precio.findElement(By.xpath("./.."));
		WebDriverWait wait=new WebDriverWait(ut.driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(parent.findElement(By.xpath(".//button"))));
		WebElement cancelar=parent.findElement(By.xpath(".//button"));
		ut.screenshot("cancelar pedido 1");
		cancelar.click();
		ut.screenshot("cancelar pedido 2");
    }
   
    public static void realizarPedido() {
    	carritoCompras();
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab3/ion-content/ion-list/ion-item[2]/button", 20);
		WebElement agregarPedido= ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab3/ion-content/ion-list/ion-item[2]/button");
		agregarPedido.click();
		ut.screenshot("realizar pedido");
    }

    public static void verPedidos() {
    	carritoCompras();
    	ut.waitObject("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab3/ion-content/ion-list/ion-list-header/button", 20);
    	WebElement verpedido=ut.objeto("/html/body/app-root/ion-app/ion-router-outlet/app-tabs/ion-tabs/div/ion-router-outlet/app-tab3/ion-content/ion-list/ion-list-header/button");
    	verpedido.click();
    	ut.screenshot("ver pedido");
    }
    
    
}


package POC.aval2.stepsDefinition;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import POC.aval2.clases.Main;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class steps {
	private static final String ACCESS_KEY = "eyJ4cC51Ijo1Mzg2OTgyLCJ4cC5wIjo1Mzg2OTgxLCJ4cC5tIjoiTVRVMU16QXdOREl6TURjek1nIiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE4NjgzNjQyMzIsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.F4VlerugZylu_Ym0WrVagvGbiwZRzeQHyANzQpDQ5nU";
	private DesiredCapabilities dc=new DesiredCapabilities();
	private URL url;
	
	
	@Given("^imprimir en consola (.*)$")
	public void imprimir_en_consola(String variable) throws Throwable {
	    System.out.println("Iniciando ejecución "+variable);
	    
	}
	
	@Then("^Agregar producto: (.*) en cantidad: (.*)$")
	public void agregarProducto(String producto, String cantidad) {
		int cantidad2=Integer.parseInt(cantidad);
		Main.agregarObjeto(producto, cantidad2);
	}
	@And("^Abrir URL local$")
	public void abrirUrlLocal() {
		Main mn=new Main();
		Main.ut.driver = new ChromeDriver();
		Main.iniciarAplicacion();
    	Main.login();
	}
	
	@And("^Abrir URL azure$")
	public void abrirURL() {
		/*Main mn=new Main();
		try {
			url = new URL("https://cloud.seetest.io/wd/hub");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	       dc.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
	       dc.setCapability(CapabilityType.PLATFORM, Platform.ANY);
	       dc.setCapability("accessKey", ACCESS_KEY);
	       dc.setCapability("testName", "Quick Start Chrome Browser Demo");
	       Main.ut.driver = new RemoteWebDriver(url, dc);
		Main.iniciarAplicacion();
    	Main.login();*/
	}
	
	@Then("^Cancelar producto: (.*)$")
	public void cancelarProducto(String producto) {
		Main.cancelarPedido(producto);
	}
	
	@Then("^Realizar pedido$")
	public void relizarPedido() {
		Main.realizarPedido();
	}
	
	@Then("^Ver pedido$")
	public void verPedido() {
		Main.verPedidos();
	}
}
